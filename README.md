# malinette-soft
Open Source Kit For Programming Interactivity. 

## Description
Standalone version of the [malinette](http://malinette.info) project. It contains Pure Data, 21 libraries, malinette-ide and a startup script. To stay up-to-date, you can replace the [malinette-ide](https://framagit.org/malinette/malinette-ide) folder.

## Installation
- Download
- Extract
- Launch "start-malinette.sh" script. Right Click > Open With > Other Application ... > Terminal

## Dependencies
You need two packages also : tcl tk. The startup script you install them for you the first time.
