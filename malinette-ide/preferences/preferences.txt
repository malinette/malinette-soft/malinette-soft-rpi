# Preferences file
# Languages french (fr) / english (en) / spanish (es)
language: fr
malinette-folder: malinette-default
malinette-rec-folder: projects/malinette-default/data
malinette-objects: preferences/malinette-objects-list.txt
brutbox-folder: brutbox-default
brutbox-rec-folder: projects/brutbox-default/data/records
brutbox-objects: preferences/brutbox-objects-list.txt
harmony: 1 major
screen: 640 480
arduino: 0
edit: 0
10bits: 0