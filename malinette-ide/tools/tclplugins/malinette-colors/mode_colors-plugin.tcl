# META NAME mode colors
# META DESCRIPTION makes edit and interact modes look different from each other
# META AUTHOR Hans-Christoph Steiner (editmode_look-plugin) + Jerome Abel (colors)
# META VERSION 0.2
# META LICENSE public domain

namespace eval ::editmode_look {
    # array of the original background colors for each window
    array set original_color {}
}

proc ::editmode_look::set_cords_by_editmode {mytoplevel} {
    variable original_color
    if {$mytoplevel eq ".pdwindow"} {return}
    set tkcanvas [tkcanvas_name $mytoplevel]
    # if the mytoplevel sent to us doesn't currently have a window, silently quit
    if { ! [winfo exists $mytoplevel] } {return}
    # if the array doesn't have this instance, get the current color
    if {[array get original_color $mytoplevel] eq ""} {
        set original_color($mytoplevel) [$tkcanvas cget -background]
    }

#### EDIT MODE ####
    if {$::editmode($mytoplevel) == 1} {
        $tkcanvas itemconfigure graph -fill black
        $tkcanvas itemconfigure array -fill black
        $tkcanvas itemconfigure atom -fill black
        $tkcanvas itemconfigure msg -fill black
        $tkcanvas itemconfigure cord -fill black
        $tkcanvas itemconfigure obj -fill "#111"
        $tkcanvas itemconfigure graph -activefill "#00f"
        $tkcanvas itemconfigure array -activefill "#00f"
        $tkcanvas itemconfigure msg -activefill "#00f"
        $tkcanvas itemconfigure atom -activefill "#00f"
        $tkcanvas itemconfigure cord -activefill "#00f"
        $tkcanvas itemconfigure {inlet || outlet} -outline "#111"
        #$tkcanvas raise {inlet || outlet || cord}
        #store the background color, in case its been changed
        #set original_color($mytoplevel) [$tkcanvas cget -background]
        $tkcanvas configure -background white
    } else {
#### ACTION MODE ####
        $tkcanvas itemconfigure graph -fill "#333"
        $tkcanvas itemconfigure array -fill "#333"
        $tkcanvas itemconfigure atom -fill "#333"
        $tkcanvas itemconfigure msg -fill "#333"
        $tkcanvas itemconfigure cord -fill "#444"
        $tkcanvas itemconfigure obj -fill "#333"
        $tkcanvas itemconfigure graph -activefill "#333"
        $tkcanvas itemconfigure array -activefill "#333"
        $tkcanvas itemconfigure msg -activefill "#f00"
        $tkcanvas itemconfigure atom -activefill "#f00"
        $tkcanvas itemconfigure cord -activefill "#444"
        #$tkcanvas itemconfigure label -fill orange
        $tkcanvas itemconfigure {inlet || outlet} -outline "#666"
        # $tkcanvas raise {inlet || outlet || cord}
        #EFFACE LES SORTIES ET ENTREES
        #$tkcanvas lower {inlet || outlet || cord}
        #$tkcanvas configure -background $original_color($mytoplevel)
        $tkcanvas configure -background "#F6F6F6"
    }
}

bind PatchWindow <<EditMode>> {+::editmode_look::set_cords_by_editmode %W}
bind PatchWindow <<Loaded>> {+::editmode_look::set_cords_by_editmode %W}