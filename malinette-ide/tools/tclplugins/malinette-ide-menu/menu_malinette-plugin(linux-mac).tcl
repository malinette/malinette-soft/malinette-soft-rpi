# META NAME menu malinette
# META DESCRIPTION display a menu for the Malinette framework
# META AUTHOR Jerome Abel
# META VERSION 0.1
# META LICENSE public domain

# DIR STRINGS
set manual_dir "../../malinette/examples/manual-"
set examples_dir "../../malinette/examples/examples-"
set ref_file "../../malinette/docs/OVERVIEW.html"
set abs_dir "../../malinette/abstractions"
set projects_dir "../../malinette/projects/default"

# TRANSLATION fr/es
::msgcat::mcset fr "Manual" "Manuel"
::msgcat::mcset es "Manual" "Manual"
::msgcat::mcset fr "Examples" "Exemples"
::msgcat::mcset es "Examples" "Ejemplos"
::msgcat::mcset fr "Reference" "Référence"
::msgcat::mcset es "Reference" "Referencia"
::msgcat::mcset fr "Objects list (html)" "Liste des objets (html)"
::msgcat::mcset es "Objects list (html)" "Lista objetos"
::msgcat::mcset fr "Objects help" "Aides des objets"
::msgcat::mcset es "Objects help" "Ayuda objetos"
::msgcat::mcset fr "Projects" "Projets"
::msgcat::mcset es "Projects" "Proyectos"

# MENUS
set malinette "malinette"
set manuel "manuel"
set example "exemple"
set ref "reference"
set objectslist "objectslist"
set projects "projects"

# LOCALIZE LANGUAGE (default:en)
set _LANG_ "en" 
if { [string match "*fr*" [::msgcat::mclocale]] } { set _LANG_ "fr" }
if { [string match "*es*" [::msgcat::mclocale]] } { set _LANG_ "es" }
append examples_dir $_LANG_
append manual_dir $_LANG_

# MALINETTE MENU
menu .menubar.$malinette
.menubar add cascade -label [_ [string totitle $malinette]] -menu .menubar.$malinette

# -- Projects
menu .menubar.$malinette.$projects
.menubar.$malinette add cascade -label [_ "Projects"] -menu .menubar.$malinette.$projects
foreach filename [glob -directory $projects_dir -nocomplain -types {f} -- *.pd] {
    .menubar.$malinette.$projects add command -label [file tail $filename] -command "open_file $filename"
}

# -- Manual
menu .menubar.$malinette.$manuel
.menubar.$malinette add cascade -label [_ "Manual"] -menu .menubar.$malinette.$manuel
foreach filename [glob -directory $manual_dir -nocomplain -types {f} -- *.pd] {
    .menubar.$malinette.$manuel add command -label [file tail $filename] -command "open_file $filename"
}

# -- Examples
menu .menubar.$malinette.$example
.menubar.$malinette add cascade -label [_ "Examples"] -menu .menubar.$malinette.$example
foreach filename [glob -directory $examples_dir -nocomplain -types {f} -- *.pd] {
    .menubar.$malinette.$example add command -label [file tail $filename] -command "open_file $filename"
}

.menubar.$malinette add separator

# -- Help objects
set menulist "in numbers seq audio video out"
foreach category $menulist {
  menu .menubar.$malinette.$category
  .menubar.$malinette add cascade -label [_ [string totitle $category]] -menu .menubar.$malinette.$category
  foreach filename [glob -directory $abs_dir/$category -nocomplain -types {f} -- *-help.pd] {
    .menubar.$malinette.$category add command -label [file tail $filename] -font "courrier 7 normal" -command "open_file $filename"
  }
}

.menubar.$malinette add  separator

# -- Objects list html
menu .menubar.$malinette.$objectslist
.menubar.$malinette add command -label [_ "Objects list (html)"] -command {menu_openfile  $ref_file}

# -- Website
.menubar.$malinette add command -label [_ "Website"] -command {menu_openfile {http://malinette.info}}
#::pdwindow::debug [::msgcat::mclocale]"\n"